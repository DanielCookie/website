local g = require 'globals'

local in_dir = "_doc/"
local out_dir = "web/doc/"

article_build_rule = {
	name = "mk_doc",
	command = "lsm $in -o $out",
	description = "Compile $out",
}

function mk_rule(template)
	local indent = "  "
	local name = "rule "..template.name
	local command = indent.."command = "..template.command
	local description = indent.."description = "..template.description
	return name.."\n"..command.."\n"..description
end

function mk_article_build_entry(article)
	local out_path = out_dir..article.basename..".html"
	local in_path = in_dir..article.basename..".lsm"
	return "build "..out_path..": "..article_build_rule.name.." "..in_path
end

buffer = mk_rule(article_build_rule).."\n"
for i=1,#g.articles
do
	buffer = buffer.."\n"..mk_article_build_entry(g.articles[i])
end
projects_build_entry = "build web/projects.html: "..article_build_rule.name.." _cfg/projects.lsm"
buffer = buffer.."\n"..projects_build_entry

print(buffer)
