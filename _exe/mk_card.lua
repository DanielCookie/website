local g = require 'globals'

local inside = ""
local line_len = 0

for line in string.gmatch(g.card, "[^\n]+")
do
	inside = inside.."| "..line.." |\n"
	line_len = string.len(line)
end

local delim = "o-"..string.rep('-', line_len).."-o"
print(delim.."\n"..inside..delim)
