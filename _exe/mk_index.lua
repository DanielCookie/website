local g = require 'globals'
local doc = require 'index'

function compose_tag(tag)
	if not tag.inner_html then -- single tag case
		if not tag.attributes then
			return '<'..tag.tag_name..'/>'
		end
		local elem = '<'..tag.tag_name
		for k,v in pairs(tag.attributes) do
			elem = elem..' '..k..'="'..v..'"'
		end
		elem = elem..'/>'
		return elem
	end

	local open_tag = '<'..tag.tag_name
	local close_tag = '</'..tag.tag_name..'>'
	if tag.attributes then
		for k,v in pairs(tag.attributes) do
			open_tag = open_tag..' '..k..'="'..v..'"'
		end
	end
	open_tag = open_tag..'>'

	if type(tag.inner_html) == "string" then
		return open_tag..tag.inner_html..close_tag
	end

	local elem = ""
	for _,nested_elem in pairs(tag.inner_html) do
		elem = elem..(type(nested_elem) == "string"
			and nested_elem
			or compose_tag(nested_elem))
 	end

	return open_tag..elem..close_tag
end

function check_correctness()
	for i,art in pairs(g.articles)
	do
		if art.last_edited.d < 1 or 31 < art.last_edited.d then
			fatal("Last edited day for article '"..art.name.."' is not between 1 and 31") end
		if art.last_edited.m < 1 or 12 < art.last_edited.m then
			fatal("Last edited month for article '"..art.name.."' is not between 1 and 12") end
		if art.last_edited.y < 0 then
			fatal("Last edited year for article '"..art.name.."' cannot be negative") end

		if art.written.d < 1 or 31 < art.written.d then
			fatal("Written day for article '"..art.name.."' is not between 1 and 31") end
		if art.written.m < 1 or 12 < art.written.m then
			fatal("Written month for article '"..art.name.."' is not between 1 and 12") end
		if art.written.y < 0 then
			fatal("Written year for article '"..art.name.."' cannot be negative") end

		-- if not lfs.attributes(art.basepath) then
			-- fatal("File '"..art.basepath.."' does not exist")
		-- end

		for i=1,#art.tags do
			local matches = false
			for j=1,#g.tag_set do
				if art.tags[i] == g.tag_set[j] then
					matches = true
					break
				end
			end
			if not matches then
				fatal("Tag '"..art.tags[i].."' for article '"..art.title.."' not in global tags list")
			end
		end
	end
end

check_correctness()

local html_comment = "<!DOCTYPE html>"
local struct = doc.get_html_structure(g)

print(html_comment..compose_tag(struct))
