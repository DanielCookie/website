require 'os'
require 'io'
require 'lfs'

function fatal(msg)
	io.stderr:write(msg.."\n")
	os.exit(1)
end

g_config_dir = lfs.currentdir() .. "/_cfg"
if not lfs.attributes(g_config_dir) then
	fatal("config dir _cfg does not exist")
end

if not lfs.attributes(g_config_dir.."/globals.lua") then
	fatal("config file _cfg/globals.lua does not exist")
end

package.path = g_config_dir.."/?.lua"
