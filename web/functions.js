function set_all_articles_visible() {
	let articles = document.getElementsByClassName("article-entity");
	if (articles.length === 0) {
		console.log("There are no articles with the class 'article-entity'");
		return;
	}
	for (let i = 0; i < articles.length; i++) {
		const article = articles[i];
		if (article.hasAttribute("disabled")) {
			article.removeAttribute("disabled");
		}
	}
}

function should_article_show_given_enabled_tags(article, enabled_tags) {
	const article_tags = article.getElementsByTagName("tag");
	for (let j = 0; j < article_tags.length; j++) {
		const article_tag = article_tags[j].innerHTML;
		for (let k = 0; k < enabled_tags.length; k++) {
			const enabled_tag = enabled_tags[k];
			if (article_tag === enabled_tag) {
				return true;
			}
		}
	}
	return false;
}

function filter_articles_per_tag(filter_tag) {
	const filtering_elem = document.getElementById("tag-" + filter_tag);
	console.assert(filtering_elem !== null);

	const tag_is_enabled = filtering_elem.hasAttribute("enabled");
	if (tag_is_enabled) {
		filtering_elem.removeAttribute("enabled");
		console.log("tag: " + filtering_elem.innerHTML + ", set disabled");
	}
	else {
		filtering_elem.setAttribute("enabled", "true");
		console.log("tag: " + filtering_elem.innerHTML + ", set enabled");
	}

	const global_tag_list = document.getElementById("taglist");
	if (global_tag_list === null) {
		console.log("No element with id 'taglist'");
		return;
	}
	const global_tags_arr = global_tag_list.getElementsByTagName("button");

	const enabled_tags = [];
	for (let i = 0; i < global_tags_arr.length; i++) {
		const tag = global_tags_arr[i];
		if (tag.hasAttribute("enabled")) {
			enabled_tags.push(tag.innerHTML);
		}
	}

	if (enabled_tags.length === 0) {
		set_all_articles_visible();
		console.log("Set all articles visible");
		return;
	}

	console.log("Set articles visibility based on enabled tags");
	let articles = document.getElementsByClassName("article-entity");
	if (articles.length === 0) {
		console.log("There are no articles with the class 'article-entity'");
		return;
	}
	for (let i = 0; i < articles.length; i++) {
		const article = articles[i];
		if (should_article_show_given_enabled_tags(article, enabled_tags)) {
			if (article.hasAttribute("disabled")) {
				article.removeAttribute("disabled")
			}
		}
		else {
			if (!article.hasAttribute("disabled")) {
				article.setAttribute("disabled", "true")
			}
		}
	}
}
