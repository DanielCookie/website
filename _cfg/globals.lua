M = {}

M.front_img = "res/avatar.png"
M.card_cmd = [[$ curl https://danielcoto.xyz/card]]
M.card     = [[= Daniel Coto =                   ]]
     .."\n"..[[email:    **********************  ]]
     .."\n"..[[website:  https://danielcoto.xyz  ]]
     .."\n"..[[mastodon: **********************  ]]

M.places = {
	{ name = "projects", link = "projects.html" },
	{ name = "mastodon", link = "https://fosstodon.org" },
}

M.tag_set = {
	'physics','science','opinion','anime'
}

M.articles = {
	{
		title = "Inertia - A Tale of Resistance and Impulse",
		basename = "inertia_a_tale_of_resistance_and_impulse",
		written = {d = 20, m = 11, y = 2020},
		last_edited = {d = 20, m = 11, y = 2020},
		tags = {'physics','science'}
	},
}

return M;
