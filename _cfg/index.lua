M = {}

M.get_html_structure = function(globals)
local g = globals

local html_head = {
	tag_name = "head",
	inner_html = {
		{
			tag_name = "title",
			inner_html = "Daniel Coto",
		},{
			tag_name = "link",
			attributes = {
				rel = "icon",
				type = "image/png",
				href = "res/favicon.png"
			}
		},{
			tag_name = "link",
			attributes = {
				rel = "stylesheet",
				type = "text/css",
				href = "index.css"
			}
		},{
			tag_name = "script",
			attributes = { src = "functions.js" },
			inner_html = ""
		},{
			tag_name = "meta",
			attributes = {
				name = "viewport",
				content = "width=device-width,initial-scale=1.0"
			}
		},{
			tag_name = "meta",
			attributes = { charset = "utf-8" },
		}
	}
}
local portal = {
	tag_name = "div",
	attributes = { id = "portal" },
	inner_html = {
		{
			tag_name = "img",
			attributes = {
				id = "avatar",
				src = g.front_img,
				alt = "Avatar not found"
			}
		},{
			tag_name = "div",
			attributes = { id = "card-section" },
			inner_html = {
				{
					tag_name = "pre",
					attributes = { id = "card-command" },
					inner_html = g.card_cmd
				},{
					tag_name = "pre",
					attributes = { id = "info-card" },
					inner_html = g.card
				}
			}
		}
	}
}
local external_places = {
	tag_name = "div",
	attributes = { id = "places" },
	inner_html = {}
}
for i=1,#g.places do
	external_places.inner_html[i] = {
		tag_name = "place",
		inner_html = {
			{
				tag_name = "a",
				attributes = { href = g.places[i].link },
				inner_html = g.places[i].name
			}
		}
	}
end
local global_tag_set = {
	tag_name = "div",
	attributes = { id = "taglist" },
	inner_html = {}
}
for i=1,#g.tag_set do
	global_tag_set.inner_html[i] = {
		tag_name = "button",
		attributes = {
			id = "tag-"..g.tag_set[i],
			onclick = "filter_articles_per_tag('"..g.tag_set[i].."')"
		},
		inner_html = g.tag_set[i]
	}
end
local articles = {
	tag_name = "ul",
	attributes = { id = "articles" },
	inner_html = {}
}
for i=1,#g.articles do
	local date = {
		tag_name = "date",
		inner_html =
			g.articles[i].last_edited.d..'.'..
			g.articles[i].last_edited.m..'.'..
			g.articles[i].last_edited.y,
	}
	local link = {
		tag_name = "a",
		attributes = { href = "doc/"..g.articles[i].basename..".html" },
		inner_html = g.articles[i].title
	}
	local tags = {
		tag_name = "tags",
		inner_html = {}
	}
	for j=1,#g.articles[i].tags do
		tags.inner_html[j] = {
			tag_name = "tag",
			inner_html = g.articles[i].tags[j]
		}
	end
	articles.inner_html[i] = {
		tag_name = "li",
		attributes = { class = "article-entity" },
		inner_html = {date, link, tags}
	}
end
local body = {
	tag_name = "body",
	inner_html = {
		portal,
		external_places,
		{
			tag_name = "h1",
			inner_html = "Articles"
		},
		global_tag_set,
		articles
	}
}

return {
	tag_name = "html",
	attributes = { lang = "en" },
	inner_html = {html_head, body}
}
end

return M
