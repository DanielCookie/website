# Website

Repo to create my website.
All the code is licensed under MIT.
All lsm documents/articles are licensed under 
[CC-BY-SA](https://creativecommons.org/licenses/by-sa/2.0/)

```
cat _exe/pre.lua _exe/mk_index.lua | lua > web/index.html
cat _exe/pre.lua _exe/mk_card.lua  | lua > web/card
cat _exe/pre.lua _exe/mk_build.lua | lua > build.ninja
samu
```

edit `_cfg/globals.lua`.
